package com.projects.mymultifunctionalapp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.projects.mymultifunctionalapp.R;

public class MainActivity extends AppCompatActivity {

    private Button btnClick;
    protected Button btnExit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getLayoutObjects();
        setLayoutObjects();

    }


    private void getLayoutObjects() {
        btnClick = findViewById(R.id.btnClick);
        btnExit = findViewById(R.id.btnExit);
    }

    private void setLayoutObjects() {
        btnClick.setOnClickListener(btnOnClickListener);
        btnExit.setOnClickListener(btnOnClickListener);
    }

    private final View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final int id = v.getId();
            Intent myIntent;
            if (id == R.id.btnClick) {
                myIntent = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(myIntent);
            } else if (id == R.id.btnExit) {
                myIntent = new Intent(getApplicationContext(), MainActivity.class);
                myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // myIntent.putExtra("EXIT", true);
                startActivity(myIntent);
                finish();
                System.exit(0);
            }
        }
    };


}