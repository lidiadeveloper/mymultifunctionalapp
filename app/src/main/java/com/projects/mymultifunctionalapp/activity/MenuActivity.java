package com.projects.mymultifunctionalapp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.projects.mymultifunctionalapp.R;

public class MenuActivity extends Activity {

    private Button btnWebView;
    private Button btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        getLayoutObjects();
        setLayoutObjects();

    }

    private void getLayoutObjects() {
        btnWebView = findViewById(R.id.btnWebView);
        btnBack = findViewById(R.id.btnBack);
    }

    private void setLayoutObjects() {
        btnWebView.setOnClickListener(btnClickListener);
        btnBack.setOnClickListener(btnClickListener);
    }

    private final View.OnClickListener btnClickListener = (View v) -> {

        final int id = v.getId();
        Intent myIntent;
        if (id == R.id.btnWebView) {
            myIntent = new Intent(MenuActivity.this, WebViewActivity.class);
            startActivity(myIntent);
        } else if (id == R.id.btnBack) {
            finish();

        }
    };


}
